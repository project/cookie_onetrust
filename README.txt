INTRODUCTION
------------

Cookie Onetrust provides purpose-built tools to help website owners and agencies simplify compliance with global privacy laws including GDPR, CCPA, 
and ePrivacy and is powered by the OneTrust platform. OneTrust was named a leader in the Forrester New Wave™: GDPR and Privacy Management Software, Q4 2018.
More info about OneTrust at https://www.onetrust.com/

REQUIREMENTS
------------

* The Cookie Onetrust module requires an Ontrust account, you need to go to https://www.onetrustpro.com/ to purchase and register an account.
  Log in to the platform via https://app-eu.onetrust.com/auth/login and select the name of the script you want under the cookies/script-integration route.
  Click to enter, you can choose a test script or a production script, copy this script

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extend/installing-modules
   for further information. 

 * Or use `drush en cookie_onetrust` from the command line or enable the module
   Via the "Extensions" admin interface. 

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions

   - Administer Cookie Onetrust settings 

     Allows users to administer Cookie Onetrust settings.

   - Bypass OneTrust

     Allows users to use the site without giving Cookie Onetrust cookie consent.  

 * Add Cookie Onetrust scripts in settings on the Administer ->
   Configuration -> Development ->  Cookie Onetrust Settings.

 * Add configuration cookie Onetrust scripts for other languages
   Administration -> Configuration -> Development -> Cookie Onetrust Settings -> Translate